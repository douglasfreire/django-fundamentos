from django.shortcuts import render, redirect
from django.http import HttpResponse
import datetime
from .models import Transação
from .form import TransacaoForm

# Create your views here.

def home(request):

    return render(request, 'contas/home.html')

def listagem(request):
    data = {}
    data ['transacoes'] = Transação.objects.all()
    return render(request, 'contas/listagem.html', data)

def nova_transacao(request):
    form = TransacaoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('url_listagem')
    return render(request, 'contas/form.html', {'form': form})

def update(request, pk):
    transacao = Transação.objects.get(pk=pk)
    form = TransacaoForm(request.POST or None, instance=transacao)
    if form.is_valid():
        form.save()
        return redirect('url_listagem')
    return render(request, 'contas/form.html', {'form': form, 'transacao': transacao})

def delete(request, pk):
    transacao = Transação.objects.get(pk=pk)
    transacao.delete()
    return redirect('url_listagem')